from tkinter import *
import main

# Init
window = Tk()
window.title("New game")
window.geometry("400x500")


"""
Function : Open game
Use : Play button
Description : Quit current window and load game's interface
"""


def open_game():
    print("@.com".endswith(".com"))
    print("@.com ".endswith(".com"))
    print("@.comdr".endswith(".com"))
    print("@.com mdr".endswith(".com"))
    window.quit()
    main.open_game(value.get())


# Text input
Label(window, text="Enter your name").pack()
value = StringVar()
value.set("Nickname")
Entry(window, textvariable=value, width=30).pack()

# HARD CODED HIGH SCORES CAUSE NO DB
textarea = Text(window)
textarea.insert(INSERT, "Virginie                -              9999999999")
textarea.insert(INSERT, "Virginie                -              9999999998")
textarea.insert(INSERT, "Virginie                -              9999999997")
textarea.insert(INSERT, "Virginie                -              9999999996")
textarea.insert(INSERT, "Virginie                -              9999999995")
textarea.insert(INSERT, "Virginie                -              9999999994")
textarea.insert(INSERT, "Flora                   -              0015391529")
textarea.insert(INSERT, "Marion                  -              0014291356")
textarea.insert(INSERT, "Nickname                -              0000128512")
textarea.insert(INSERT, "Nickname                -              0000125618")
textarea.insert(INSERT, "Nickname                -              0000118512")
textarea.insert(INSERT, "Nickname                -              0000115618")
textarea.insert(INSERT, "Nickname                -              0000108512")
textarea.insert(INSERT, "Nickname                -              0000105618")
textarea.insert(INSERT, "Nickname                -              0000098512")
textarea.insert(INSERT, "Nickname                -              0000095618")
textarea.insert(INSERT, "Antoine                 -              0000000000")
textarea.pack()

# Redirect
button = Button(window,
                text="Play !",
                command=open_game)
button.pack()

window.mainloop()
