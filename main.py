from random import randrange
from tkinter import *
from time import *
from _thread import *

# Player default values
player = {"hp": 3,
          "pos": 0}
# Projectile default values
projectilePos = {"x": 3,
                 "y": 0}


"""
Function : Up
Use : Tk bind()
Description : Move the player upwards
"""


def up(key):
    if player["pos"] != 0:
        player["pos"] -= 1

    print(player)


"""
Function : Down
Use : Tk bind()
Description : Move the player downwards
"""


def down(key):
    if player["pos"] != 3:
        player["pos"] += 1

    print(player)


"""
Function : Game loop
Use : Main open_game()
Description : Do the main game loop
@:parameter playerModel : Player Label
@:parameter projectileModel : Projectile Label
@:parameter delay : Delay in s between game loops
@:parameter distance : Distance modulo for projectile spawning
@:parameter score : Score value
@:parameter start : Start perf_counter() values for elapsed time
"""


def game_loop(playerModel, projectileModel, delay, distance, score, start):
    while player["hp"] > 0:
        # Wait .3s
        elapsed = perf_counter() - start

        # Display Player model
        try:
            playerModel.grid(row=player["pos"])
        except:
            print(player)

        # Display Projectile model
        try:
            projectileModel.grid(row=projectilePos["y"], column=projectilePos["x"])
        except:
            print(projectilePos)
            print(distance)

        # Projectile
        if elapsed >= delay:
            start = perf_counter()
            # Hit, -1 HP
            if projectilePos["x"] <= 1 and projectilePos["y"] == player["pos"]:
                # HP
                player["hp"] -= 1
                # Ease game for each hit
                distance += 5
                print("Hit !")

            # Reset projectile
            if projectilePos["x"] <= 1:
                # Reset position by a random number
                projectilePos["y"] = randrange(3)
                # Reset x position by a random number and the distance modulo
                projectilePos["x"] = randrange(3, 4)+distance

                # Harden game for each reset
                if distance < 5:
                    distance = 0
                else:
                    distance -= 5

                print("Reset")
            # Advance projectile
            else:
                projectilePos["x"] -= 1
                print("Move Projectile | x : " + str(projectilePos["x"]) + " y :" + str(projectilePos["y"]))

            # Score
            score += 10-distance

    # Game over
    print("You lost !")
    return 0


"""
Function: Open Game
Use : Log open_game()
Description : Load the main game window
@:parameter nickname : Player's nickname sent from last window
"""


def open_game(nickname):
    # Init window
    play = Tk()
    play.bind("<Key-Up>", up)
    play.bind("<Key-Down>", down)
    play.title(nickname + "\'s game")

    # Set viewport size from bg image
    bg = PhotoImage(master=play, file="BG.gif")
    w = bg.width()
    h = bg.height()
    play.geometry("%dx%d+0+0" % (w, h))

    # BG
    background_label = Label(play, image=bg)
    background_label.grid(row=0, column=0, rowspan=4, columnspan=4)

    # Load models
    playerImage = PhotoImage(master=play, file="Character.gif")
    playerModel = Label(play, image=playerImage)
    projectileImage = PhotoImage(master=play, file="Projectile.gif")
    projectileModel = Label(play, image=projectileImage)

    # Game loop
    delay = .35
    distance = 10
    score = 0
    start = perf_counter()

    # Threads
    try:
        start_new_thread(game_loop, (playerModel, projectileModel, delay, distance, score, start))
        play.mainloop()
    except:
        print("Unable to start thread")

    play.quit()
